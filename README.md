# CarCar
CarCar is a software solution designed to oversee the operations of a car dealership. It handles the stock management, car sales, and car services.
Team:

* Charles Agar - Service
* Scott Passmore - Sales

## How to Run this App
1. add a manufacturer, model, and automobile to inventory. In that order.
    a. for service add a tech and appointment in that order.
        -appointments displays appointments and when the buttons are clicked they disappear. VIP have VIP next to VIN (vin is in inventory).
        -search vin in appointments by VIN for record of all services to that VIN. Delete will delete that VIN from service history.

## Diagram
CarCar consists of three interconnected microservices:

Inventory Management
Service Handling
Sales Processing
![alt text](image.png)

## API Documentation
1. inventory:
        Inventory Front End
            Manufacturer
                - Should have a working form view ![alt text](image-11.png)
                - Should have a working list view  ![alt text](image-12.png)
            Vehicle Model
                - Should have a working form view ![alt text](image-13.png)
                - Should have a working list view ![alt text](image-14.png)
            Automobile
                - Should have a working form view ![alt text](image-15.png)
                - Should have a working list view![alt text](image-16.png)
## Service microservice
2. service:
     Backend:
        Technicians Resource:
            GET request to API should respond with a list of technicians: ![alt text](image-1.png)
            POST request to API should create a new technician resource: ![alt text](image-2.png)
        Appointments Resource:
           GET request to API should respond with a list of appointments: ![alt text](image-3.png)
           POST request to API should create a new appointment: ![alt text](image-4.png)
    Frontend:
        Navigation:
            Should have a link to an Add a Technician Form View: ![alt text](image-5.png)
            Should have a link to an Add a Service Appointment Form View: ![alt text](image-6.png)
            Should have a link to Show a List of Appointments View ![alt text](image-7.png)
            Should have a link to Show Service Appointments by VIN View(optional)![alt text](image-8.png)
        Add a Technician Form:
            Should contain relevant inputs 1
            - Should contain an Employee Name input
            - Should contain an Employee ID input
                On submit should create a new technician ![alt text](image-10.png)
                No JS console errors ![alt text](image-9.png)
        Add a Service Appointment Form
         Should contain relevant inputs 1
            - Should contain a VIN input
            - Should contain the vehicle owner's name input
            - Should contain an input for the appointment date and time
            - Should contain an input to select a service technician
            - Should contain an input to add a reason for service appt
            On submit should create a new service appointment 3
            No JS console errors 1
            Verify state updates inside of list of appointments/service appts by VIN ![alt text](image-6.png)
        Service Appointments List
            Should contain a list of the following information for each service appointment 1
            - VIN
            - Customer Name
            - Date and Time
            - Assigned Technician Name
            - Reason for Service
            If the vehicle was purchased from the dealer's inventory, then should be a visual indicator of "VIP treatment" 3
            Should have a button to complete/finish a service appointment 1
            - On click should "complete" the appointment and remove the appointment from the view 2.5
            Should have a button to cancel a service appointment 1
            - On click should cancel the appointment and remove the appointment from the view 2.5
            No JS console errors![alt text](image-7.png)
        Show Service History by VIN(separate view or as part of service appointments list)
          Should contain a VIN input 1
            Should contain a submit button 1
             On submit, should show the service history for that VIN(vehicle) 2
            - Information displayed should only include appointments for that VIN (ie. the filter works) 3
            - This information should be displayed without needing to refresh the page 2
            Should (at minimum) contain a list of the following information for each service appointment 1
            - Customer Name
            - Date and Time
            - Assigned Technician Name
            - Reason for Service
            No JS console errors![alt text](image-8.png)

## Sales microservice
3. sales:
    Front End
        Navigation![alt text](image-28.png)
            Should have a link to an Add Sales Person Form View
            Should have a link to an Add a Customer Form View
            Should have a link to a Create a Sale Record Form View
            Should have a link to a List All Sales View
            Should have a link to a List All Sales by Sales Person View (optional)
        Add Sales Person Form View (5%)![alt text](image-27.png)
            Should contain relevant inputs
            - should contain a name input
            - should contain an employee number input
            On submit, should create a new sales person
            No JS console errors
            Verify state updates inside Create a Sale Record View
        Add a Customer Form View (5%)![alt text](image-26.png)
            Should contain relevant inputs
            - should contain a name input
            - should contain an address input
            - should contain a phone number input
            On submit, should create a new potential customer
            No JS console errors
            Verify state updates inside Create a Sale Record View
        Create a Sale Record Form View (10%)![alt text](image-25.png)
          Should contain relevant inputs
            - should contain a dropdown to select an unsold Automobile in inventory
            - should contain a dropdown to select a Sales Person
            - should contain a dropdown to select a Customer
            - should contain a sale price input
            On submit, should create a new sale record
            No JS console errors
            Verify state updates inside List All Sales View
        List All Sales View (5%)![alt text](image-24.png)
            Should contain the following information for each record
            - Sales Person Name
            - Employee Number
            - Purchaser Name
            - Automobile VIN
            - Price of Sale
            No JS Console Errors
        List Sales History by Sales Person (5%)![alt text](image-23.png)
            Should contain a dropdown to select a sales person by name or employee number?
            On selection, should display a list of sales records for that sales person without refreshing
            Should contain the following information for each record
            - Sales Person Name
            - Customer Name
            - Automobile VIN
            - Sale Price
            No JS Console Errors
    Back End
       Sales Person Resource
        GET request to API should respond with a list of sales people
        GET request to API should respond with an appropriate status code![alt text](image-21.png)
        POST request to API should create a new sales person resource
        POST request to API should respond with an appropriate status code![alt text](image-22.png)
    Customer Resource
        GET request to API should respond with a list of customers![alt text](image-19.png)
        GET request to API should respond with an appropriate status code
        POST request to API should create a new customer resource![alt text](image-20.png)
        POST request to API should respond with an appropriate status code
    Sales Resource
        GET request to API should respond with a list of sales![alt text](image-17.png)
        GET request to API should respond with an appropriate status code
        POST request to API should create a new sales resource![alt text](image-18.png)
        POST request to API should respond with an appropriate status code
