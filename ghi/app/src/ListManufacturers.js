import { useEffect, useState } from 'react';
import CreateManufacturer from './CreateManufacturer';

function ListManufacturers() {
  const [manufacturers, setManufacturers] = useState([]);
  const [showForm, setShowForm] = useState(false);

  const getData = async () => {
    const response = await fetch('http://localhost:8100/api/manufacturers/');

    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleFormSubmit = () => {
    setShowForm(false);
    getData();
  };

  return (
    <div>
      <button onClick={() => setShowForm(true)}>Create New Manufacturer</button>
      {showForm && <CreateManufacturer onFormSubmit={handleFormSubmit} />}
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers.map(manufacturer => (
            <tr key={manufacturer.id}>
              <td>{manufacturer.name}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ListManufacturers;
