import React from 'react';

const SalespeopleItem = ({salesperson}) => {
    return (
        <tr>
            <td>{salesperson.employee_id}</td>
            <td>{salesperson.first_name}</td>
            <td>{salesperson.last_name}</td>
        </tr>
    );
};

export default SalespeopleItem;
