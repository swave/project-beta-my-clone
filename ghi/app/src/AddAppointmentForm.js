import React, { useState, useEffect } from 'react';

function AddAppointmentForm() {
  const [formData, setFormData] = useState({
    customer: '',
    vin: '',
    reason: '',
    status: '',
    employee_id: '', 
    date_time: '',
  });

  const [technicians, setTechnicians] = useState([]);

  useEffect(() => {
    fetch('http://localhost:8080/api/technicians/')
      .then(response => response.json())
      .then(data => setTechnicians(data.technicians));
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = 'http://localhost:8080/api/appointments/';

    const fetchConfig = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        ...formData,
        date_time: new Date().toISOString(),
      }),
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setFormData({
        customer: '',
        vin: '',
        reason: '',
        status: '',
        employee_id: '',
        date_time: '',
      });
      alert('Appointment added successfully!');
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Customer Name:
        <input type="text" value={formData.customer} onChange={handleFormChange} name="customer" required />
      </label>
      <label>
        VIN:
        <input type="text" value={formData.vin} onChange={handleFormChange} name="vin" required />
      </label>
      <label>
        Reason:
        <input type="text" value={formData.reason} onChange={handleFormChange} name="reason" required />
      </label>
      <label>
        Status:
        <input type="text" value={formData.status} onChange={handleFormChange} name="status" required />
      </label>
      <label>
        Technician:
        <select value={formData.employee_id} onChange={handleFormChange} name="employee_id" required>
          <option value="">Select a technician</option>
          {technicians.map(technician => (
            <option key={technician.employee_id} value={technician.employee_id}>
              {technician.first_name} {technician.last_name}
            </option>
          ))}
        </select>
      </label>
      <label>
      Date and Time:
      <input type="datetime-local" value={formData.date_time} onChange={handleFormChange} name="date_time" required />
    </label>
      <button type="submit">Add Appointment</button>
    </form>
  );
}

export default AddAppointmentForm;
