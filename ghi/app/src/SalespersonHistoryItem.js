import React from 'react';

const SalespersonHistoryItem = ({sale}) => {
    return (
        <tr>
            <td>{sale.salesperson.first_name}</td>
            <td>{sale.customer.first_name}</td>
            <td>{sale.automobile.vin}</td>
            <td>{sale.price}</td>
        </tr>
    );
}

export default SalespersonHistoryItem;
