import React from 'react';

const SalesItem = ({sale}) => {
    return (
        <tr>
            <td>{sale.salesperson.employee_id}</td>
            <td>{sale.salesperson.first_name}</td>
            <td>{sale.customer.first_name}</td>
            <td>{sale.automobile.vin}</td>
            <td>{sale.price}</td>
        </tr>
    );
}

export default SalesItem;
