import React, { useState, useEffect } from 'react';

function CreateAVehicleModel() {
  const [name, setName] = useState('');
  const [pictureUrl, setPictureUrl] = useState('');
  const [manufacturerId, setManufacturerId] = useState('');
  const [manufacturers, setManufacturers] = useState([]);

  useEffect(() => {
    const fetchManufacturers = async () => {
      const response = await fetch('http://localhost:8100/api/manufacturers/');

      if (response.ok) {
        const data = await response.json();
        setManufacturers(data.manufacturers);
      }
    };

    fetchManufacturers();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const response = await fetch('http://localhost:8100/api/models/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        name,
        picture_url: pictureUrl,
        manufacturer_id: manufacturerId,
      }),
    });

    if (response.ok) {
      alert('Vehicle model created successfully!');
    } else {
      alert('Could not create the vehicle model');
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Name:
        <input type="text" value={name} onChange={(e) => setName(e.target.value)} />
      </label>
      <label>
        Picture URL:
        <input type="text" value={pictureUrl} onChange={(e) => setPictureUrl(e.target.value)} />
      </label>
      <label>
        Manufacturer:
        <select value={manufacturerId} onChange={(e) => setManufacturerId(e.target.value)}>
          <option value="">Select a manufacturer</option>
          {manufacturers.map(manufacturer => (
            <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
          ))}
        </select>
      </label>
      <button type="submit">Create Vehicle Model</button>
    </form>
  );
}

export default CreateAVehicleModel;
