import React, { useState, useEffect } from 'react';


const CreateNewSaleForm = function() {
    const [salespeople, setSalespeople] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [automobiles, setAutomobiles] = useState([]);
    const [sale, setSale] = useState({
        salesperson: '',
        customer: '',
        automobile: '',
        price: ''
    });

    const getSalespeople = async function() {
        const url = 'http://localhost:8090/api/salespeople';
        const response = await fetch(url);
        const data = await response.json();
        setSalespeople(data.salespeople);
    }

    const getCustomers = async function() {
        const url = 'http://localhost:8090/api/customers';
        const response = await fetch(url);
        const data = await response.json();
        setCustomers(data.customers);
    }

    const getAutomobiles = async function() {
        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);
        const data = await response.json();

        setAutomobiles(data.autos);
    }

    const handleFormSubmit = async function(event) {
        event.preventDefault();
        const url = 'http://localhost:8090/api/sales/';
        const response = await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(sale)
        });
        const data = await response.json();
        console.log(data);
    }


    const handleInputChange = function(event) {
        setSale({
            ...sale,
            [event.target.name]: event.target.value
        });
    }

    useEffect(() => {
        getSalespeople();
        getCustomers();
        getAutomobiles();
    }, []);

    console.log(sale);

        return (
            <>
                <h1>Create New Sale</h1>
                <form onSubmit={handleFormSubmit}>
                    <div className="form-group">
                        <label htmlFor="salesperson">Salesperson</label>
                        <select className="form-control" name="salesperson" onChange={handleInputChange}>
                            <option value="">Select a salesperson</option>
                            {salespeople.map(salesperson => <option key={salesperson.id} value={salesperson.first_name}>{salesperson.first_name} {salesperson.last_name}</option>)}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="customer">Customer</label>
                        <select className="form-control" name="customer" onChange={handleInputChange}>
                            <option value="">Select a customer</option>
                            {customers.map(customer => <option key={customer.id} value={customer.first_name}>{customer.first_name} {customer.last_name}</option>)}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="automobile">Automobile VIN</label>
                        <select className="form-control" name="automobile" onChange={handleInputChange}>
                            <option value="">Select an automobile</option>
                            {automobiles && automobiles.map((auto) => (
                                <option key={auto.id} value={auto.vin}> {auto.vin}</option>
                            ))}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="price">Sale Price</label>
                        <input type="number" min="0" step="0.01" className="form-control" name="price" onChange={handleInputChange} />
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </>
        );
    }

    export default CreateNewSaleForm;
