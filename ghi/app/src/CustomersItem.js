import React from 'react';

const CustomersItem = ({customer}) => {
    return (
        <tr>
            <td>{customer.first_name}</td>
            <td>{customer.last_name}</td>
            <td>{customer.phone_number}</td>
            <td>{customer.address}</td>
        </tr>
    );
};

export default CustomersItem;
