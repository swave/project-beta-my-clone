import React, { useState, useEffect } from 'react';
import SalespersonHistoryItem from './SalespersonHistoryItem';

const SalespersonHistoryList = function() {
    const [sales, setSales] = useState([]);
    const [salespeople, setSalespeople] = useState([]);
    const [selectedSalesperson, setSelectedSalesperson] = useState(null);

    const getSalespeople = async function() {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url);
        const data = await response.json();
        setSalespeople(data.salespeople);
        if (data.salespeople.length > 0) {
            setSelectedSalesperson(data.salespeople[0].employee_id);
        }
    }

    const getSales = async function() {
        if (selectedSalesperson) {
            const url = `http://localhost:8090/api/sales/?salesperson=${selectedSalesperson.employee_id}`;
            console.log(`Fetching sales for salesperson ${selectedSalesperson.employee_id} from ${url}`);
            const response = await fetch(url);
            const data = await response.json();
            console.log('Received sales data:', data);
            setSales(data);

        }
    }

    useEffect(() => {
        getSalespeople();
    }, []);

    useEffect(() => {
        if (selectedSalesperson) {
            getSales();
        }
    }, [selectedSalesperson]);
    console.log('selectedSalesperson:', selectedSalesperson);
    // selectedSalesperson ? selectedSalesperson.employee_id : null

    const filteredSales = sales.filter(sale => sale.salesperson.id === parseInt(selectedSalesperson));

    console.log('filteredSales:', filteredSales);

    return (
        <>
            <h1>Salesperson History</h1>
            <select onChange={e => setSelectedSalesperson(e.target.value)}>
                {salespeople.map(salesperson => <option key={salesperson.id} value={salesperson.id}>{salesperson.first_name}</option>)}
            </select>
            {filteredSales.length > 0 ?
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>Vin</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredSales.map(sale => <SalespersonHistoryItem key={sale.id} sale={sale} />)}
                </tbody>
            </table>
            : <p>Sorry No Sales Exist!😂</p>}

        </>
    )
}

export default SalespersonHistoryList;
