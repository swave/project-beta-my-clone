import React, { useState } from 'react';

function CreateManufacturer({ onFormSubmit }) {
  const [formData, setFormData] = useState({
    name: '',
  });

  const handleSubmit = async (event) => {
    event.preventDefault();

    const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(manufacturerUrl, fetchConfig);

    if (response.ok) {
      setFormData({
        name: '',
      });
      onFormSubmit();
    } else {
      alert('Could not create the manufacturer');
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Name:
        <input type="text" value={formData.name} onChange={handleFormChange} name="name" />
      </label>
      <button type="submit">Create Manufacturer</button>
    </form>
  );
}

export default CreateManufacturer;
