import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

# Import models from sales_rest, here.
# from sales_rest.models import Something
from sales_rest.models import AutomobileVO


def poll():
    while True:
        print('Sales poller polling for data')
        try:
            res = requests.get('http://inventory-api:8000/api/automobiles/')
            content = res.json()
            for auto in content['autos']:
                obj, created = AutomobileVO.objects.get_or_create(vin=auto['vin'])
                print(obj, created , file=sys.stderr)
                if not created and obj.sold != True:
                    obj.sold = auto['sold']
                    obj.save()

                    # AutomobileVO.objects.update_or_create(
                    #     vin=auto['vin'],
                    #     defaults={
                    #     'sold': auto['sold']
                    #     }
                    # )
        except Exception as e:
            print(e, file=sys.stderr)

        time.sleep(60)


if __name__ == "__main__":
    poll()
