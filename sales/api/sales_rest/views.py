from django.shortcuts import render
from django.http import JsonResponse, HttpResponse, HttpResponseBadRequest
from django.views.decorators.http import require_http_methods
import json, requests
from .models import Sale, Salesperson, Customer, AutomobileVO
from .encoders import SaleEncoder, SalespersonEncoder, CustomerEncoder, AutomobileVOEncoder
from django.forms.models import model_to_dict

# Create your views here.



@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            sales,
            encoder=SaleEncoder,
            safe=False,
        )

    else:  # POST
        try:
            content = json.loads(request.body)
            automobile_vin = content["automobile"]
            automobile_instance = AutomobileVO.objects.get(vin=automobile_vin)

            if automobile_instance.sold:
                return JsonResponse({"error": "AutomobileVO is already sold."}, status=400)

            customer_name = content["customer"]
            customer_instance = Customer.objects.get(first_name=customer_name)

            salesperson_name = content["salesperson"]
            salesperson_instance = Salesperson.objects.get(first_name=salesperson_name)

            price = float(content["price"])


            automobile_instance.sold = True
            automobile_instance.save()
            sales = Sale.objects.create(price=price, customer=customer_instance, salesperson=salesperson_instance, automobile=automobile_instance)


            # Use AutomobileVOEncoder to serialize the automobile_instance
            automobile_data = AutomobileVOEncoder().default(automobile_instance)

            return JsonResponse({
                "message": "Sale created successfully",
                "sale": model_to_dict(sales),
                "automobile": automobile_data  # Include the serialized automobile data in the response
            }, status=201)

        except AutomobileVO.DoesNotExist:
            return JsonResponse({"error": "AutomobileVO does not exist."}, status=400)







@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        salespeople_list = [model_to_dict(salesperson) for salesperson in salespeople]
        return JsonResponse(
            #{"salespeople": salespeople},
            {"salespeople": salespeople_list},
            #encoder=SalespersonEncoder,
        )
    else:#POST
        try:
            content = json.loads(request.body)
            #salespeople_id = content["salespeople_id"]
            #salesperson = Salesperson.objects.get(pk=salespeople_id)
            #content["salespeople"] = salesperson
            model = Salesperson.objects.create(**content)
            return JsonResponse(
                model,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the salespeople"}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE"])
def api_detail_salespeople(request, pk = None):
    if request.method == "DELETE":
        deleted, _ = Salesperson.objects.filter(id=pk).delete()
        return JsonResponse(
            {"deleted": deleted > 0},
        )


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        #return HttpResponse("POST request received.")
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the customer"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE"])
def api_detail_customer(request, pk = None):
    if request.method == "DELETE":
        deleted, _ = Customer.objects.filter(id=pk).delete()
        return JsonResponse(
            {"deleted": deleted > 0},
        )
