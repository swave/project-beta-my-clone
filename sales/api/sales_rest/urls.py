from django.urls import path
from .views import api_list_sales, api_list_salespeople, api_list_customers, api_detail_salespeople, api_detail_customer

urlpatterns = [
    path("sales/", api_list_sales, name="api_list_sales"),
    path("salespeople/", api_list_salespeople, name="list_salespeople"),
    path("customers/", api_list_customers, name="list_customers"),
    path("salespeople/<int:pk>/", api_detail_salespeople, name="api_detail_salespeople"),
    path("customers/<int:pk>/", api_detail_customer, name="api_detail_customer")
]
